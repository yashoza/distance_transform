//C++ Implementation - Path planning using Distance transform

//Adding necessary header files 
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <cmath>
#include<vector>

//defining PI = 3.14
#define PI 3.1412

//Global variables WORLD_SIZE, WORLD_W, WORLD_H
//hold the size, height and width of the map 
//WORLD_SIZE is defined as width*height
int WORLD_SIZE, WORLD_W, WORLD_H;

//std and cv namespace for opencv functions 
using namespace cv;
using namespace std;

//BoundsCheck functions checks if a given point 
//lies on the map or not
//returs true if it does 
bool BoundsCheck(int,bool);

//StructCell structure holds information about each
//cell on the map -
//coordinates, index and parent 
struct SearchCell
{
    //public variables 
    public:
        
        int m_xcoord, m_zcoord;
        int m_id;
        SearchCell * parent;
        float G;
        float H;

        //constructor definition - parent = 0 assigned 
        //for the initial node 
        SearchCell()
        {
            parent =0;
        }

        //for all other nodes, the following parameters
        //are initialized 
        SearchCell(int x, int z, SearchCell * _parent)
        {
            m_xcoord = x;
            m_zcoord = z;
            parent = _parent;
            m_id = z*WORLD_SIZE + x;
            G=0;
            H=0;
        }

        //returns the F cost of each cell
        //this holds the heuristic value
        float GetF()
        {
            return H;
        }

        //manhattan distance for current cell to goal cell
        float ManHattanDistance(SearchCell * nodeEnd)
        {
            float x = (float)(fabs((float)(this->m_xcoord- nodeEnd->m_xcoord)));
            float z = (float)(fabs((float)(this->m_zcoord- nodeEnd->m_zcoord)));
            return sqrt(x*x + z*z);
        }
};

//PathFinding class - holds all the member functions
//takes in a map; generates path based on the given 
//cost function. maintains a list of points which have
//been covered, yet to be covered, and are currently 
//being considered for evaluation
class PathFinding
{
    //all the required variables are defined here 
    public:
        bool m_intializedStartGoal;
        bool m_foundGoal;
        SearchCell *m_startCell;
        SearchCell *m_goalCell;
        vector<SearchCell*> m_openList;
        vector<SearchCell*> m_visitedList;
        vector<Point3f> m_pathToGoal;
        Mat SearchSpace;

        //constructor definition
        PathFinding(Mat graph)
        {
            SearchSpace = graph;
            m_intializedStartGoal = false;
            m_foundGoal = false;
        }

        //this function takes in the current position and 
        //the target position, and generates a path between them. 
        //calls the method ContinuePath() every time new 
        //neighbours are being considered. 
        void FindPath(Point3f currentPos, Point3f targetPos)
        {
            if(!m_intializedStartGoal)
            {
                m_openList.erase(m_openList.begin(),m_openList.end());
                m_visitedList.erase(m_visitedList.begin(),m_visitedList.end());
                m_pathToGoal.erase(m_pathToGoal.begin(),m_pathToGoal.end());

                SearchCell start;
                start.m_xcoord = currentPos.x;
                start.m_zcoord = currentPos.z;
                SearchCell goal;
                goal.m_xcoord = targetPos.x;
                goal.m_zcoord = targetPos.z;
                SetStartAndGoal(start,goal);
                m_intializedStartGoal = true;
            }

            while((!m_foundGoal) && !(m_openList.empty()))
            {
                ContinuePath();
            }

            //if openList is empty, that implies that no path is found 
            if(m_openList.empty())
            {
                cout<<">>Found No Path to Goal: Terminating"<<endl;
                cout<<">>Reset the Box Size"<<endl;
            }

        }

        //this function is for initializing start and goal points 
        void SetStartAndGoal(SearchCell start,SearchCell goal)
        {
            m_startCell = new SearchCell(start.m_xcoord,start.m_zcoord,NULL);
            m_goalCell = new SearchCell(goal.m_xcoord,goal.m_zcoord,NULL);//Made a change from &goal
            m_startCell->G=0;
            m_startCell->H= m_startCell->ManHattanDistance(m_goalCell);
            m_startCell->parent = 0;
            m_openList.push_back(m_startCell);
        }

        //this function checks if the cell has already been visited
        //by itrating through the visited list  
        void PathOpened(int x,int z,float newCost,SearchCell* parent)
        {
            
            if(CellBCheck(x,z))
            {
                return;
            }
            
            int id = z * WORLD_SIZE + x;
            for(int i=0;i< m_visitedList.size();i++)
            {
                if(id == m_visitedList[i]->m_id)
                {
                    return;
                }
            }

            SearchCell * newChild = new SearchCell(x,z,parent);
            newChild->G = newCost;
            newChild->H = newChild->ManHattanDistance(m_goalCell);
            m_openList.push_back(newChild);
        }
      
        //gets next cell from the list of cells that are being considered for 
        //evaluation
        SearchCell *GetNextCell()
        {
            float bestF = 9999999.0f;
            int cellIndex = -1;
            SearchCell * nextCell = NULL;
            for(int i=0; i < m_openList.size();i++)
            {
                if(m_openList[i]->GetF()< bestF)
                {
                    bestF = m_openList[i]->GetF();
                    cellIndex = i;
                }
            }

            if(cellIndex>=0)
            {
                nextCell = m_openList[cellIndex];
                m_visitedList.push_back(nextCell);
                m_openList.erase(m_openList.begin()+ cellIndex);
            }
            return nextCell;
        }
        
        //this function checks the distance of the pixel from the obstacles.
        //it does not consider the ones that are at a distance less than 
        //11 pixels away from the obstacles, in essence assigning a cost of 
        //infinity to them. 
        bool CellBCheck(int x,int z)
        {
            if((x<SearchSpace.size().width && x>=0) && (z<SearchSpace.size().height && z>=0))
            {        
               if( (int)SearchSpace.at<uchar>(z,x)>10	)
               {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }

        }
        vector<Point3f> RetPlan()
        {
            return m_pathToGoal;
        }

        //this function considers all the neighbours of the given parent 
        //cell. It considers the 8 surrounding neighbours on the grid, 
        //and evaluates their cost. based on the least cost, it uses the 
        //appropriate cell
        void ContinuePath()
        {

            SearchCell * currentCell = GetNextCell();
            if(currentCell->m_id == m_goalCell->m_id)
            {
                SearchCell* getPath;
                for(getPath = currentCell; getPath!= NULL; getPath= getPath->parent)
                {
                    m_pathToGoal.push_back(Point3f(getPath->m_xcoord,0,getPath->m_zcoord));
                }
                m_foundGoal = true;
                return;
            }
            else
            {
                //rightCell
                PathOpened(currentCell->m_xcoord+1,currentCell->m_zcoord,currentCell->G+1,currentCell);
                //leftCell
                PathOpened(currentCell->m_xcoord-1,currentCell->m_zcoord,currentCell->G+1,currentCell);
                //topCell
                PathOpened(currentCell->m_xcoord,currentCell->m_zcoord+1,currentCell->G+1,currentCell);
                //bottomCell
                PathOpened(currentCell->m_xcoord,currentCell->m_zcoord-1,currentCell->G+1,currentCell);
                //diagnaltopLeft
                PathOpened(currentCell->m_xcoord-1,currentCell->m_zcoord+1,currentCell->G+1.414,currentCell);
                //diagnaltopRight
                PathOpened(currentCell->m_xcoord+1,currentCell->m_zcoord+1,currentCell->G+1.414,currentCell);
                //diagnalbottomLeft
                PathOpened(currentCell->m_xcoord-1,currentCell->m_zcoord-1,currentCell->G+1.414,currentCell);
                //diagnalbottomRight
                PathOpened(currentCell->m_xcoord+1,currentCell->m_zcoord-1,currentCell->G+1.414,currentCell);

                for(int i=0; i < m_openList.size();i++)
                {
                    if(currentCell->m_id == m_openList[i]->m_id)
                    {
                        m_openList.erase(m_openList.begin()+i);
                    }
                }

            }
        }
};

//main function
int main(int argc, char ** argv)
{
    //defining necessary Mat type images 
    Mat src,gray,thresholded;
    src = imread(argv[1], 0);
    
    //list of points 
    vector<Point2i> map_point;
    vector<Point3f> plan_point;
    Size s = src.size();

    WORLD_SIZE = s.height*s.width;
    WORLD_W = s.width;
    WORLD_H = s.height;

    //cout<<s.height<<"\t"<<s.width<<endl;

    //creating an object of class PathFinding called Astar
    PathFinding Astar(src); 

    namedWindow("MyImage",CV_WINDOW_NORMAL);

    //checking if proper image has been supplied 
    if(!src.data)
    {
        cerr<<"No Image Supplied"<<endl;
        return -1;
    }

    //pusing points onto the map_point list if they pass
    //the boundcheck condition 
    for(int i=0;i<s.width;i+=1)
    { 
        for(int j=0;j<s.height;j+=1)
        {
            if(BoundsCheck(i,0) && BoundsCheck(j,1))
            {
                map_point.push_back(Point2i(i,j));
            }
        }
    }

    //finding the path from point a to point b 
    Astar.FindPath(Point3f(140,0,200),Point3f(725,0,1095));//140,0,200  62,0,14

    //getting, plotting and storing the path 
    plan_point = Astar.RetPlan();
    if(plan_point.size()>0)
    {
        cout<<">>Optimal Path Detected:"<<endl;
        for(int i=0;i<plan_point.size();i++)
        {
            Point2i temp = map_point[int((plan_point[i].x*(WORLD_H))+plan_point[i].z)];
            circle(src,Point(temp.x,temp.y),2,Scalar(250,250,250),-1, 8, 0 );           
        }
    }

    //displaying the output 
    imshow("MyImage",src);
    waitKey();
    return 0;
}

//fucntion to check if the poit lies inside the boundary 
bool BoundsCheck(int x,bool y)
{
    if(y)
    {
        if((x)>WORLD_H)
            return 0;
        else
            return 1;
    }
    else
    {
        if((x)>WORLD_W) 
            return 0;
        else
            return 1;
    }
}

